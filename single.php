

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hello Bulma!</title>
    <link href="https://unpkg.com/tailwindcss/dist/tailwind.min.css" rel="stylesheet">
    <script type="text/javascript">
    /*<![CDATA[*/

    (function () {
			
      var scriptURL = 'https://sdks.shopifycdn.com/buy-button/latest/buy-button-storefront.min.js';
      if (window.ShopifyBuy) {
        if (window.ShopifyBuy.UI) {
          ShopifyBuyInit();
        } else {
          loadScript();
        }
      } else {
        loadScript();
      }

      function loadScript() {
        var script = document.createElement('script');
        script.async = true;
        script.src = scriptURL;
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
        script.onload = ShopifyBuyInit;
      }

      function ShopifyBuyInit() {
        var client = ShopifyBuy.buildClient({
          domain: 'thesummitlighthousestore.myshopify.com',
          storefrontAccessToken: '7dd0d61bef823bb747fda3fd2bada7b5',
        });

        

        ShopifyBuy.UI.onReady(client).then(function (ui) {
          // Buy Button customization: https://shopify.github.io/buy-button-js/customization/
          var options = {
            product: {
              variantId: "all",
              width: "240px",
              contents: {
                img: false,
                imgWithCarousel: false,
                title: false,
                variantTitle: false,
                price: true,
                description: false,
                buttonWithQuantity: false,
                quantity: false
              },
              text: {
                button: "ADD TO CART"
              },
              styles: {
                product: {
                  "@media (min-width: 601px)": {
                    "max-width": "100%",
                    "margin-left": "0",
                    "margin-bottom": "50px"
                  }
                },
                button: {
                  "background-color": "#993CBD",
                  "padding-left": "20px",
                  "padding-right": "20px",
                  ":hover": {
                    "background-color": "#993CBD"
                  },
                  "border-radius": "100px",
                  ":focus": {
                    "background-color": "#b946cf"
                  }
                },
                price: {
                  "font-size": "18px",
                },
                compareAt: {
                  "font-size": "12px"
                }
              }
            },
            cart: {
              startOpen: false,
              popup: false,
              contents: {
                button: true
              },
              styles: {
                button: {
                  "background-color": "#cd4ee6",
                  ":hover": {
                    "background-color": "#b946cf"
                  },
                  "border-radius": "100px",
                  ":focus": {
                    "background-color": "#b946cf"
                  }
                },
                footer: {
                  "background-color": "#ffffff"
                }
              }
            },
            modalProduct: {
              contents: {
                img: false,
                imgWithCarousel: true,
                variantTitle: false,
                buttonWithQuantity: true,
                button: false,
                quantity: false
              },
              styles: {
                product: {
                  "@media (min-width: 601px)": {
                    "max-width": "100%",
                    "margin-left": "0px",
                    "margin-bottom": "0px"
                  }
                },
                button: {
                  "background-color": "#cd4ee6",
                  "padding-left": "20px",
                  "padding-right": "20px",
                  ":hover": {
                    "background-color": "#b946cf"
                  },
                  "border-radius": "100px",
                  ":focus": {
                    "background-color": "#b946cf"
                  }
                }
              }
            },
            toggle: {
              styles: {
                toggle: {
                  "background-color": "#cd4ee6",
                  ":hover": {
                    "background-color": "#b946cf"
                  },
                  ":focus": {
                    "background-color": "#b946cf"
                  }
                },
                count: {
                  color: "#ffffff",
                  ":hover": {
                    color: "#ffffff"
                  }
                },
                iconPath: {
                  fill: "#ffffff"
                }
              }
            },
            productSet: {
              styles: {
                products: {
                  "@media (min-width: 601px)": {
                    "margin-left": "-20px"
                  }
                }
              }
            }
          };

          // Show Product Buy Button Component //
          ui.createComponent('product', {
            id: ["<?php the_field('shopify_product_id'); ?>"],
            node: document.getElementById('my-product'),
            moneyFormat: '%24%7B%7Bamount%7D%7D',
            options
          });
        });
      }
    })();
    /*]]>*/
    </script>

  </head>
  <body>
    <div class="container mx-auto">  
      <h1 class="title">  
        Single Shopify product id: <?php the_field('shopify_product_id'); ?>
      </h1>   
      <br>
      <!-- Product Buy Button Component -->
      <div id="my-product"></div>
    </div>
</body>
</html>