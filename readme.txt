=== Shopify Buy Button ===


Our 2019 default theme is designed to show off the power of the block editor.

== Description ==
Shopify Buy Button lets merchants generate an embeddable product card and checkout that can be placed on any type of website. With Advanced Custom Fields PRO we can add one time custom javascript code and add the specific product ID code to display the ADD TO CART Button in the single product posts.

== Resources ==
* Shopify Buy Button
* Advanced Custom Fields PRO
